#ifndef CONFIG_H
#define CONFIG_H

#define MAX_STACK_SIZE 0x1000 // stack size defined in startup_LPC17xx.s
#define MAX_THREAD_COUNT 16 // maximum thread that can be created
#define THREAD_STACK_SIZE 0x100 // stack size for each thread
#define TIMER_PERIOD_US 1000 // 1ms period
#define MAX_SEMAPHORE_COUNT (MAX_THREAD_COUNT)
#define TIME_FOREVER (~0ULL)

#endif
