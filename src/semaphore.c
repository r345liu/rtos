#include "kernel_core.h"
#include "thread_core.h"
#include "semaphore.h"
#include "config.h"
#include "syscall.h"

#include "string.h"

Semaphore semaphores[MAX_SEMAPHORE_COUNT];

static uint32_t get_q_next(uint32_t curr) {
	return (curr + 1) % SEMAPHORE_Q_MAX;
}

static uint32_t get_q_prev(uint32_t curr) {
	return (curr + SEMAPHORE_Q_MAX - 1) % SEMAPHORE_Q_MAX;
}

void semaInit(void) {
	memset(semaphores, 0, sizeof(semaphores));
}

semaphore_t semaCreate(unsigned initial, unsigned max, int owner_release) {
	semaphore_t sema;

	// look for the first non-allocated semaphore from static list
	for(sema = 0; sema < MAX_SEMAPHORE_COUNT; sema++) {
		if(!semaphores[sema].allocated) {
			break;
		}
	}

	if(semaphores[sema].allocated) {
		// no more avaiable semaphores
		return INVALID_SEMAPHORE;
	}

	memset(semaphores[sema].blocked_q, 0, sizeof(semaphores[sema].blocked_q));
	semaphores[sema].owner = INVALID_THREAD;
	semaphores[sema].q_start = 0;
	semaphores[sema].q_end = 0;
	semaphores[sema].tokens = initial > max ? max : initial;
	semaphores[sema].max_tokens = max;
	semaphores[sema].owner = INVALID_THREAD;
	semaphores[sema].owner_release = owner_release;
	semaphores[sema].allocated = 1;

	return sema;
}

int semaDelete(semaphore_t sema) {
	if(sema < MAX_SEMAPHORE_COUNT) {
		if(semaphores[sema].q_end == semaphores[sema].q_start) {
			// no threads in the queue, deallocate
			semaphores[sema].allocated = 0;
			return 0;
		}
		// there are threads in the queue still
		return 1;
	}
	return -1;
}

int semaTake(semaphore_t sema, uint64_t timeout) {
	if(sema < MAX_SEMAPHORE_COUNT && semaphores[sema].allocated) {
		int syscall_ret = syscall(SYSCALL_SEMAPHORE_TAKE, sema, timeout);
		if(syscall_ret == 1) {
			// thread was blocked then unblocked
			if(threads[current_thread].blocked_by == sema) {
				// was a time out
				threads[current_thread].blocked_by = INVALID_SEMAPHORE;
				return 1;
			}

			// was a notify
			return 0;
		}
		return syscall_ret;
	}

	return -1;
}

int semaGive(semaphore_t sema) {
	if(sema < MAX_SEMAPHORE_COUNT && semaphores[sema].allocated) {
		return syscall(SYSCALL_SEMAPHORE_GIVE, sema);
	}
	
	return -1;
}

int sysHandlerSemaTake(semaphore_t sema, uint64_t timeout) {
	if(sema < MAX_SEMAPHORE_COUNT && semaphores[sema].allocated) {
		uint32_t q_next = get_q_next(semaphores[sema].q_end);
		if(semaphores[sema].tokens > 0) {
			// semaphore has token, no blocking
			semaphores[sema].tokens--;

			// set owner for when owner test on release mode is enabled
			if(semaphores[sema].tokens == 0) {
				semaphores[sema].owner = current_thread;
			}

			return 0;

		} else if(q_next == semaphores[sema].q_start) {
			// no space in the thread queue
			return -1;

		} else {
			// add thread to queue and block it
			semaphores[sema].blocked_q[semaphores[sema].q_end] = current_thread;
			semaphores[sema].q_end = q_next;
			threads[current_thread].release_counter = timeout;
			threads[current_thread].state = BLOCKED;
			threads[current_thread].blocked_by = sema;
			kernelSchedule();
			return 1;
		}
	}

	return -1;
}

int sysHandlerSemaGive(semaphore_t sema) {
	if(sema < MAX_SEMAPHORE_COUNT && semaphores[sema].allocated) {
		if(semaphores[sema].owner != INVALID_THREAD) {
			if(semaphores[sema].owner_release && semaphores[sema].owner != current_thread) {
				// not the owner of the lock, error
				return -1;
			}
		}

		if(semaphores[sema].q_start != semaphores[sema].q_end) {
			// there are threads blocking in queue, unqueue one and unblock
			thread_t unblocked = semaphores[sema].blocked_q[semaphores[sema].q_start];
			semaphores[sema].q_start = get_q_next(semaphores[sema].q_start);
			threads[unblocked].state = WAITING;
			threads[unblocked].release_counter = threads[unblocked].period;
			threads[unblocked].blocked_by = INVALID_SEMAPHORE;
			semaphores[sema].owner = unblocked;
		} else if(semaphores[sema].tokens < semaphores[sema].max_tokens) {
			// no threads in queue, release owner and increment token
			semaphores[sema].owner = INVALID_THREAD;
			semaphores[sema].tokens++;
		}

		return 0;
	}

	return -1;
}

void semaUnqueue(semaphore_t sema, thread_t thread) {
	int found = 0;
	if(sema < MAX_SEMAPHORE_COUNT && semaphores[sema].allocated) {
		uint32_t i;
		for(i = semaphores[sema].q_start; i != semaphores[sema].q_end; i = get_q_next(i)) {
			if(semaphores[sema].blocked_q[i] == thread) {
				found = 1;
			} else if(found == 1) {
				semaphores[sema].blocked_q[i-1] = semaphores[sema].blocked_q[i];
			}
		}
		if(found) {
			semaphores[sema].q_end = get_q_prev(semaphores[sema].q_end);
		}
	}
}
