#ifndef THREAD_CORE_H
#define THREAD_CORE_H

#include "config.h"
#include <stdint.h>

#define INVALID_THREAD (~0) // used to indicate errors

typedef uint32_t thread_t; // type alias to represent thread id
typedef void (*thread_func_t)(void*);

#include "semaphore.h"

typedef enum ThreadState {
	UNINIT = 0, // uninitialixed thread
	STOPPED,    // created but not started
	RUNNING,    // currently running thread, same thread pointed by current_thread
	ACTIVE,     // active threads that are not running
	WAITING,    // thread waiting for next release
	BLOCKED,    // thread blocked by semaphore
} ThreadState;

typedef struct Thread {
	uint64_t period;
	uint64_t release_counter;
	uint32_t *sp;
	semaphore_t blocked_by;
	ThreadState state;
} Thread;

extern Thread threads[MAX_THREAD_COUNT];
extern thread_t current_thread;

// function to initialize thread's internal data structures
void threadInit(void);

// returns initial MSP location stored in address 0x0
uint32_t *getMSPInitialLocation(void);

// returns a stack address associated with given thread id.
// Returns NULL when out of memory
uint32_t *threadGetStack(thread_t thread);

// allocates a new thread, returns id of new thread. Period is in microseconds
thread_t threadCreate(thread_func_t thread_func, uint64_t period, void *user_data);

// update the thread state to running
void threadStart(thread_t thread);

// yields to another thread
void threadYield(void);

// sleep for microseconds
void threadSleep(uint64_t micros);

// scheduler function, returns the thread id of next thread to run
thread_t threadScheduleNext(void);

// house keeping function to call every SysTick, updates counters of each thread
void threadTick(void);

// set PSP to stack address of current_thread
// called from svc_call.s, returns 1 when success
int threadTaskSwitch(void);

void sysHandlerThreadSleep(uint64_t);

#endif
