#include "thread_core.h"
#include "kernel_core.h"
#include "semaphore.h"
#include "syscall.h"
#include "config.h"

#include <LPC17xx.h>
#include <stddef.h>
#include <string.h>

Thread threads[MAX_THREAD_COUNT];
thread_t current_thread;

void threadInit(void) {
	current_thread = INVALID_THREAD; // reset current thread;
	memset(threads, 0, sizeof(threads));
}

uint32_t *getMSPInitialLocation(void) {
	return *((uint32_t * volatile *) 0x0);
}

uint32_t *threadGetStack(thread_t thread) {
	uint32_t init_sp = (uint32_t) getMSPInitialLocation();
	uint32_t new_sp = init_sp - (thread+1) * THREAD_STACK_SIZE; // offset 0 is reserved for MSP

	// expand stack to 8 byte alignment
	new_sp = new_sp & ~7;

	if(init_sp - new_sp > MAX_STACK_SIZE) {
		// stack size exceeds max stack size
		return NULL;
	}

	return (uint32_t*) new_sp;
}

thread_t threadCreate(thread_func_t thread_func, uint64_t period, void *user_data) {

	thread_t i;
	thread_t next_thread = INVALID_THREAD;

	uint32_t *sp;

	// find next available thread
	for(i = 0; i < MAX_THREAD_COUNT; i++) {
		if(threads[i].state == UNINIT) {
			next_thread = i;
			break;
		}
	}

	if(next_thread == INVALID_THREAD) {
		// no more memory for thread
		return INVALID_THREAD;
	}

	// setup stack pointer
	sp = threadGetStack(next_thread);
	*--sp = 1 << 24; // xPSR, set thumb state bit to 1
	*--sp = (uint32_t) thread_func; // PC
	*--sp = 0xCE; // LR
	*--sp = 0xCC; // R12
	*--sp = 0xC3; // R3
	*--sp = 0xC2; // R2
	*--sp = 0xC1; // R1
	*--sp = (uint32_t) user_data; // R0
	*--sp = 0xCB; // R11
	*--sp = 0xCA; // R10
	*--sp = 0xC9; // R9
	*--sp = 0xC8; // R8
	*--sp = 0xC7; // R7
	*--sp = 0xC6; // R6
	*--sp = 0xC5; // R5
	*--sp = 0xC4; // R4

	// setup thread struct
	threads[next_thread].state = STOPPED;
	threads[next_thread].sp = sp;
	threads[next_thread].period = period;
	threads[next_thread].blocked_by = INVALID_SEMAPHORE;

	return next_thread;
}

void threadStart(thread_t thread) {
	if(thread < MAX_THREAD_COUNT) {
		// set thread to WAITING but counter to 0 so it gets started right a way by scheduler
		threads[thread].release_counter = 0;
		threads[thread].state = WAITING;
	}
}

void threadYield(void) {
	if(current_thread < MAX_THREAD_COUNT) {
		syscall(SYSCALL_SLEEP, 0ULL);
	}
}

void threadSleep(uint64_t micros) {
	if(current_thread < MAX_THREAD_COUNT) {
		syscall(SYSCALL_SLEEP, micros);
	}
}

thread_t threadScheduleNext(void) {
	thread_t i, next_thread = INVALID_THREAD;
	uint64_t earliest = ~0ULL;
	ThreadState next_thread_state = UNINIT;

	for(i = 0; i < MAX_THREAD_COUNT; i++) {
		// update thread states and counter
		if(threads[i].release_counter == 0) {
			switch(threads[i].state) {
				case BLOCKED: // semaphore timed out
					semaUnqueue(threads[i].blocked_by, i);

					// fallthrough
				case WAITING: // period passed
					// release thread
					threads[i].state = ACTIVE;

					// fallthrough
				case RUNNING:
				case ACTIVE:
					threads[i].release_counter = threads[i].period;
					break;

				default:
					// do nothing
					break;
			}
		}

		// find thread with earliest deadline
		if(threads[i].state == ACTIVE) {
			if(
				threads[i].release_counter < earliest ||
				// prioritize threads that are not running when a tie happens
				(threads[i].release_counter == earliest && next_thread_state != WAITING)
			) {
				earliest = threads[i].release_counter;
				next_thread = i;
				next_thread_state = threads[i].state;
			}
		}
	}

	return next_thread;
}

void threadTick(void) {
	thread_t i;
	// update thread counters
	for(i = 0; i < MAX_THREAD_COUNT; i++) {
		if(threads[i].release_counter < TIME_FOREVER) {
			if(threads[i].release_counter > TIMER_PERIOD_US) {
				threads[i].release_counter -= TIMER_PERIOD_US;
			} else {
				threads[i].release_counter = 0;
			}
		}
	}
}

int threadTaskSwitch(void) {
	if(current_thread < MAX_THREAD_COUNT) {
		__set_PSP((int32_t) threads[current_thread].sp);
		return 1;
	}
	return 0;
}

void sysHandlerThreadSleep(uint64_t micros) {
	if(current_thread < MAX_THREAD_COUNT) {
		if(micros > threads[current_thread].release_counter) {
			// set counter to sleep duration
			threads[current_thread].release_counter = micros;
		}
		threads[current_thread].state = WAITING; // end current period
		kernelSchedule(); // call scheduler
	}
}
