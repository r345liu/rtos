// defines the syscall numbers
// using constant instead of enums so it can be used in asm instruction

#ifndef SYSCALL_H
#define SYSCALL_H

typedef enum {
	SYSCALL_SCHEDULER = 0,
	SYSCALL_SLEEP,
	SYSCALL_SEMAPHORE_TAKE,
	SYSCALL_SEMAPHORE_GIVE,
	SYSCALL_MAX = 0xFFFFFFF,
} SysCall;

// defined in svc_call.s
// triggers system call with correct arguments placements to the handler
// SVC_Handler should receive all the arguments that fit in r0-r3 registers
// ie., 32bit padded arguments < 4 words total
int syscall(SysCall call, ...);

#endif
