#ifndef SEMAPHORE_H
#define SEMAPHORE_H

#include "thread_core.h"
#include "config.h"

#include <stdint.h>

#define INVALID_SEMAPHORE (~0)

typedef struct {
	thread_t blocked_q[MAX_THREAD_COUNT+1];
	thread_t owner;
	uint32_t q_start, q_end;
	unsigned tokens, max_tokens;
	int owner_release;
	int allocated;
} Semaphore;

#define SEMAPHORE_Q_MAX (sizeof(semaphores[0].blocked_q) / sizeof(semaphores[0].blocked_q[0]))

typedef uint32_t semaphore_t;

extern Semaphore semaphores[MAX_SEMAPHORE_COUNT];

// initialize semaphore module
void semaInit(void);

// create a semaphore
// initial: initial token count
// max:     maximum token count
// owner_release: whether only the owner of semaphore can unblock
// return: created semaphore, or INVALID_SEMAPHORE on error
semaphore_t semaCreate(unsigned initial, unsigned max, int owner_release);

// delete a semaphore, can only be used when no threads are blocked by it
// returns 0 on success, 1 when there are threads blocked, -1 when error
int semaDelete(semaphore_t);

// take one token from semaphore, calls syscall internally
// returns 0 when success, 1 when timed out, -1 when error
int semaTake(semaphore_t, uint64_t timeout);

// give one token to semaphore, calls syscall internally
// when owner_release is 1, only the thread that took the last token can unblock
// return 0 on success, -1 on error
int semaGive(semaphore_t);

// functions to be called by system call handler

// syscall handler for taking semaphore token
// returns 0 when no blocking, 1 when blocked, -1 when error
int sysHandlerSemaTake(semaphore_t, uint64_t timeout);

// syscall handler for giving semaphore token
// returns 0 when succss, -1 when error
int sysHandlerSemaGive(semaphore_t);

// unqueue thread from a semaphore, should only be called from handler to avoid race condition
void semaUnqueue(semaphore_t, thread_t);

#endif
