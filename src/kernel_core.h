#ifndef KERNEL_CORE_H
#define KERNEL_CORE_H

#include "syscall.h"
#include <stdint.h>

// initialized handler priorities
void kernelInit(void);

// start kernel, does not return
void kernelStart(void);

// call the scheduler and switch thread, should only be called from handler
void kernelSchedule(void);

// millis since kernelStart
uint64_t kernelMicros(void);

// handler for SysTick timer
void SysTick_Handler(void);

// main handler for SVC, called from svc_call.s
int SVC_Handler_main(SysCall syscall, ...);

// idle thread to do nothing
void idleThread(void*);

#endif
