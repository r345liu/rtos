	area handle_pend,code,readonly
	extern threadTaskSwitch ; defined in thread_core.c
	extern SVC_Handler_main ; defined in kernel_core.c
	global PendSV_Handler
	global SVC_Handler
	global syscall
	preserve8

PendSV_Handler
	mrs r0, psp           ; load psp to r0
	stmdb r0, {r4-r11}    ; push r4-r11 to psp

	push {lr,r12}         ; store lr, r12 is to pad and maintain stack alignment
	bl threadTaskSwitch   ; change psp to new thread
	pop {lr,r12}          ; restore lr

	cbz r0, pendsv_return ; task_switch failed, return without switching

	mrs r0, psp           ; load new psp to r0
	ldmia r0!, {r4-r11}   ; pop r4-r11
	msr psp, r0           ; store popped address back to sp

pendsv_return
	bx lr                 ; return

syscall
	; triggers svc handler with properly stored arguments
	svc #0
	bx lr

SVC_Handler
	; pop arguments from psp
	; not actually needed because r0-r3 aren't changed on exception entry
	;mrs r12, psp        ; load psp to r12
	;ldmia r12!, {r0-r3} ; pop from r12

	; call main handler function
	; pushing 2 registers to maintain stack alignment
	push {lr,r12}       ; store lr, r12 is to pad and maintain stack alignment
	bl SVC_Handler_main ; call
	pop {lr,r12}        ; restore lr

	; push return values to stack
	tst lr, #4          ; check if thread or handler triggered svc
	ite eq
	mrseq r12, msp      ; handler mode, load msp
	mrsne r12, psp      ; thread mode, load psp
	add r12, #0x10      ; rewrite the last 4 words
	stmdb r12!, {r0-r3} ; push to r12 (which is the stack)

	bx lr               ; return

	end
