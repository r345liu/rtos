#include "thread_core.h"
#include "kernel_core.h"
#include "semaphore.h"
#include "config.h"

#include <LPC17xx.h>
#include <stdio.h>
#include <stdint.h>

static semaphore_t
	btn_push_sema    = INVALID_SEMAPHORE,
	print_mutex      = INVALID_SEMAPHORE,
	x_mutex          = INVALID_SEMAPHORE,
	led_mutex        = INVALID_SEMAPHORE;

static int print_stuff = 0;
static int x = 0;

// helper function to set leds to a 8 bit number
static void setLED(uint8_t x) {
	uint8_t gpio1 = (x & 1<<7) << 21 | (x & 1<<6) << 23 | (x & 1<<5) << 26;
	uint8_t gpio2 = (x & 1<<4) >> 2 | (x & 1<<3) | (x & 1<<2) << 2 | (x & 1<<1) << 4 | (x & 1) << 6;
	LPC_GPIO1->FIOCLR = ~gpio1;
	LPC_GPIO2->FIOCLR = ~gpio2;
	LPC_GPIO1->FIOSET = gpio1;
	LPC_GPIO2->FIOSET = gpio2;
}

// thread that polls for button, and notifies the btn_push_sema at rising edge
static void pollButtonThread(void *args) {
	static int last_btn = 0;
	int btn;
	while(1) {
		btn = !((LPC_GPIO2->FIOPIN >> 10) & 0x1);
		if(btn && !last_btn) {
			semaGive(btn_push_sema);
		}
		last_btn = btn;
		threadYield();
	}
}

// thread that waits on the btn_push_sema, prints message, and toggles print_stuff
static void showButtonThread(void *args) {
	while(1) {
		if(semaTake(btn_push_sema, 10000000) == 0) {
			if(semaTake(print_mutex, TIME_FOREVER) == 0) {
				print_stuff ^= 1;
				puts("button pushed");
				semaGive(print_mutex);
			}
		} else {
			if(semaTake(print_mutex, TIME_FOREVER) == 0) {
				printf("button semaphore timed out %llu\n", kernelMicros());
				semaGive(print_mutex);
			}
		}
	}
}

// threads that print stuff protected by print_mutex
static void printThread(void *msg) {
	while(1) {
		if(semaTake(print_mutex, TIME_FOREVER) == 0) {
			if(print_stuff) {
				printf("%s: %llu\n", (char*) msg, kernelMicros());
			}
			semaGive(print_mutex);
		}
		threadYield();
	}
}

// thread that increment led
static void ledThread0(void *args) {
	while(1) {
		if(semaTake(x_mutex, TIME_FOREVER) == 0) {
			x++;
			semaGive(x_mutex);
		}
		threadYield();
	}
}

// thread that sets led to x % 47
static void ledThread1(void *args) {
	while(1) {
		if(semaTake(led_mutex, TIME_FOREVER) == 0) {
			if(semaTake(x_mutex, TIME_FOREVER) == 0) {
				setLED(x % 47);
				semaGive(x_mutex);
			}
			semaGive(led_mutex);
		}
		threadYield();
	}
}

// thread that sets led to 0x71
static void ledThread2(void *args) {
	while(1) {
		if(semaTake(led_mutex, TIME_FOREVER) == 0) {
			setLED(0x71);
			semaGive(led_mutex);
		}
		threadYield();
	}
}

int main(void) {
	int i;
	thread_t
		poll_button_thread,
		show_button_thread,
		led_threads[3],
		print_threads[3];
	
	// configure leds GPIOs to output
	LPC_GPIO1->FIODIR = 0xB0000000; // 0b10110000000000000000000000000000;
	LPC_GPIO2->FIODIR = 0x0000007C; // 0b00000000000000000000000001111100;

	kernelInit();

	// create semaphores and muteces
	btn_push_sema = semaCreate(0, 1, 0); // semaphore
	print_mutex = semaCreate(1, 1, 1);   // mutex
	x_mutex = semaCreate(1, 1, 1);       // mutex
	led_mutex = semaCreate(1, 1, 1);     // mutex

	// create threads
	poll_button_thread = threadCreate(pollButtonThread, 100000, NULL);
	show_button_thread = threadCreate(showButtonThread, 1000, NULL);

	led_threads[0] = threadCreate(ledThread0, 100000, NULL);
	led_threads[1] = threadCreate(ledThread1, 100000, NULL);
	led_threads[2] = threadCreate(ledThread2, 100000, NULL);

	print_threads[0] = threadCreate(printThread, 5000, "Thread 1");
	print_threads[1] = threadCreate(printThread, 5000, "Thread 2");
	print_threads[2] = threadCreate(printThread, 5000, "Thread 3");

	// start threads
	threadStart(poll_button_thread);
	threadStart(show_button_thread);

	for(i = 0; i < 3; i++) {
		threadStart(print_threads[i]);
		threadStart(led_threads[i]);
	}

	kernelStart(); // no return
}
