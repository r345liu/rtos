#include "kernel_core.h"
#include "thread_core.h"
#include "semaphore.h"
#include "syscall.h"
#include "config.h"

#include <LPC17xx.h>
#include <stdint.h>
#include <string.h>
#include <stdarg.h>

#define SHPR2 (*(uint32_t*) 0xE000ED1C) // system handler priority register 3
#define SHPR3 (*(uint32_t*) 0xE000ED20) // system handler priority register 2
#define ICSR (*(uint32_t*) 0xE000ED04)  // interrupt control and state register

static uint64_t microsCounter;

static thread_t idle_thread;

void kernelInit(void) {
	SHPR2 |= 0xFDU << 24; // set SVC to highest priority
	SHPR3 |= 0xFEU << 16; // set PendSV to in between priority
	SHPR3 |= 0xFFU << 24; // set SysTick to lowest priority

	// initialize threading module
	threadInit();

	// initializing semaphore module
	semaInit();

	// create the idleThread
	idle_thread = threadCreate(idleThread, TIME_FOREVER, NULL);
	if(idle_thread == INVALID_THREAD) {
		// TODO, report error?
		while(1);
	}
}

void kernelStart(void) {
	// reset millis counter
	microsCounter = 0;

	// start systick timer
	SysTick_Config(SystemCoreClock / TIMER_PERIOD_US);

	// start the idle thread
	threadStart(idle_thread);

	// using idle_thread's stack as a place to hold unused context from first switch
	__set_PSP((int32_t) threads[idle_thread].sp);
	__set_CONTROL(__get_CONTROL() | 0x2); // sets bit 1 MSEL to 1 (use PSP for SP)
	kernelSchedule(); // trigger the scheduler

	// scheduling failed
	while(1);
}

void kernelSchedule(void) {
		thread_t next_thread = threadScheduleNext();

		if(next_thread == INVALID_THREAD) {
			// scheduling failed
			return;
		}

		if(next_thread == current_thread) {
			// no scheduling is required
			return;
		}

		if(current_thread != INVALID_THREAD) {
			// store current PSP to thread
			// subtract by 0x40 because PendSV_Handler push 8 words of context to stack
			// assuming kernelSchedule is called from a handler
			threads[current_thread].sp = (uint32_t*) (__get_PSP() - 0x20);

			// update thread state. If thread is WAITING keep it that way
			if(threads[current_thread].state == RUNNING) {
				threads[current_thread].state = ACTIVE;
			}
		}

		// switch to new thread
		current_thread = next_thread;
		threads[next_thread].state = RUNNING;

		// trigger PendSV to switch context
		ICSR |= 1 << 28; // trigger PendSV
		__asm("isb");    // flush the pipeline
}

uint64_t kernelMicros(void) {
	return microsCounter;
}

void SysTick_Handler(void) {
	// increment system timer
	microsCounter += TIMER_PERIOD_US;

	// let kernel module to house keeping, updating counter
	threadTick();

	// trigger scheduling
	kernelSchedule();
}

int SVC_Handler_main(SysCall syscall, ...) {
	semaphore_t sema_arg;

	va_list vargs;
	va_start(vargs, syscall);

	switch(syscall) {
		case SYSCALL_SCHEDULER:
			kernelSchedule();
			break;

		case SYSCALL_SLEEP:
			sysHandlerThreadSleep(va_arg(vargs, uint64_t));
			break;

		case SYSCALL_SEMAPHORE_TAKE:
			sema_arg = va_arg(vargs, semaphore_t); // extract first va_arg first to ensure order
			return sysHandlerSemaTake(sema_arg, va_arg(vargs, uint64_t));

		case SYSCALL_SEMAPHORE_GIVE:
			return sysHandlerSemaGive(va_arg(vargs, semaphore_t));

		default:
			break;
	}

	va_end(vargs);

	return 0;
}

void idleThread(void *user_data) {
	while(1);
}
